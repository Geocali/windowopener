import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
import time
import Adafruit_DHT
import configparser
import os
#import logging
import csv
from datetime import datetime

#logging.basicConfig(filename='informations.log',level=logging.DEBUG, format='%(asctime)s,%(message)s', datefmt="%Y-%m-%d %H:%M:%S")

relays_gpios = {1: 5, 2: 6, 3: 13, 4: 19}

def get_mode():
    try:
        with open("mode.txt", "r") as f:
            mode = f.readline()
    except:
        mode = "auto"
    return mode

def get_config():
    config = configparser.ConfigParser()
    config.read('parameters.ini')
    time_moving = int(config['parameters']['time_moving'])
    min_t = float(config['parameters']['min_t'])
    max_t = float(config['parameters']['max_t'])
    logfile = config['parameters']['logfile']
    return {'time_moving': time_moving, 'min_t': min_t, 'max_t': max_t, 'logfile': logfile}

def close_relay(relay_nb):
    # ==== close the relay ====
    RELAY_GPIO = relays_gpios[relay_nb]
    # GPIO Assign mode
    GPIO.setup(RELAY_GPIO, GPIO.OUT)
    # close the relay
    GPIO.output(RELAY_GPIO, GPIO.LOW)
    time.sleep(0.5)
    return

def open_relay(relay_nb):
    # ==== close the relay ====
    RELAY_GPIO = relays_gpios[relay_nb]
    # GPIO Assign mode
    GPIO.setup(RELAY_GPIO, GPIO.OUT)
    # close the relay
    GPIO.output(RELAY_GPIO, GPIO.HIGH)
    time.sleep(0.5)
    return

def read_th():
    DHT_SENSOR = Adafruit_DHT.DHT22
    DHT_PIN = 4
    humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
    return {'temperature': round(temperature, 2), 'humidity': round(humidity, 2)}

def open_window(time_moving):
    close_relay(2)
    close_relay(4)
    time.sleep(time_moving)
    open_relay(2)
    open_relay(4)
    return

def close_window(time_moving):
    close_relay(1)
    close_relay(3)
    time.sleep(time_moving + 10)
    open_relay(1)
    open_relay(3)
    return

def get_last_state(time_moving):
    
    infos = []
    if os.path.isfile("informations.csv"):
        with open('informations.csv') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                infos.append(row)
        
        window_closed = (infos[len(infos) - 1][3] == "window closed")
    else:
        with open('informations.csv', 'w') as the_file:
            the_file.write('time,temperature,humidity,window_state,action,mode')
            the_file.write("\n")
        close_window(time_moving)
        window_closed = True
    
    return window_closed

def log_new_state(new_infos, logfile):
    with open(logfile, 'a') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(new_infos)
    return

def manage():

    params = get_config()
    mode = get_mode()

    # first, we check the state of the window
    window_closed = get_last_state(params['time_moving'])
    
    # we measure humidity and temperature
    measure = read_th()
    action = "nothing"

    if mode == "auto":
        # we check if action is needed
        if (window_closed and (measure['temperature'] > params['max_t'])):
            open_window(params['time_moving'])
            window_closed = False
            action = "opened window"
        elif ((not window_closed) and (measure['temperature'] < params['min_t'])):
            close_window(params['time_moving'])
            window_closed = True
            action = "closed window"
    elif ( (mode == "close") and (not window_closed) ):
        close_window(params['time_moving'])
        window_closed = True
        action = "closed window"
    elif ( (mode == "open") and (window_closed) ):
        open_window(params['time_moving'])
        window_closed = False
        action = "opened window"

    # we log everything
    if window_closed:
        state = "window closed"
    else:
        state = "window open"

    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d %H:%M:%S")

    new_infos = [
        date_time,
        measure['temperature'],
        measure['humidity'],
        state,
        action,
        mode
    ]
    log_new_state(new_infos, params['logfile'])
    
    return new_infos

if __name__ == "__main__":
    manage()