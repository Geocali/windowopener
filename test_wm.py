import pytest
from mock import MagicMock

import window_manager as wm

relays = [1, 2, 3, 4]

@pytest.mark.skip(reason="already tested")
@pytest.mark.parametrize('relay', relays)
def test_relays(relay):
    wm.close_relay(relay)
    wm.open_relay(relay)
    assert 1 == 1

def test_dht11():
    r = wm.read_th()
    assert r['temperature'] > 0
    assert r['humidity'] > 0

def test_manage_t_high_window_closed():

    th = {'temperature': 23, 'humidity': 50}
    window_closed = True
    params = {'time_moving': 1, 'min_t': 19, 'max_t': 21}

    wm.get_config = MagicMock(return_value=params)
    wm.open_window = MagicMock(return_value=None)
    wm.close_window = MagicMock(return_value=None)
    wm.read_th = MagicMock(return_value=th)
    wm.get_last_state = MagicMock(return_value=window_closed)
    wm.log_new_state = MagicMock(return_value=None)
    
    infos = wm.manage()
    assert infos[4] == 'opened window'

def test_manage_t_high_window_open():

    th = {'temperature': 23, 'humidity': 50}
    window_closed = False
    params = {'time_moving': 1, 'min_t': 19, 'max_t': 21}

    wm.get_config = MagicMock(return_value=params)
    wm.open_window = MagicMock(return_value=None)
    wm.close_window = MagicMock(return_value=None)
    wm.read_th = MagicMock(return_value=th)
    wm.get_last_state = MagicMock(return_value=window_closed)
    wm.log_new_state = MagicMock(return_value=None)
    
    infos = wm.manage()
    
    assert infos[4] == 'nothing'

def test_manage_t_low_window_closed():

    th = {'temperature': 2, 'humidity': 50}
    window_closed = True
    params = {'time_moving': 1, 'min_t': 19, 'max_t': 21}

    wm.get_config = MagicMock(return_value=params)
    wm.open_window = MagicMock(return_value=None)
    wm.close_window = MagicMock(return_value=None)
    wm.read_th = MagicMock(return_value=th)
    wm.get_last_state = MagicMock(return_value=window_closed)
    wm.log_new_state = MagicMock(return_value=None)
    
    infos = wm.manage()
    
    assert infos[4] == 'nothing'

def test_manage_t_low_window_open():

    th = {'temperature': 2, 'humidity': 50}
    window_closed = False
    params = {'time_moving': 1, 'min_t': 19, 'max_t': 21}

    wm.get_config = MagicMock(return_value=params)
    wm.open_window = MagicMock(return_value=None)
    wm.close_window = MagicMock(return_value=None)
    wm.read_th = MagicMock(return_value=th)
    wm.get_last_state = MagicMock(return_value=window_closed)
    wm.log_new_state = MagicMock(return_value=None)
    
    infos = wm.manage()
    
    assert infos[4] == 'closed window'
