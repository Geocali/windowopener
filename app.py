import window_manager as wm
import time
from flask import Flask, redirect, url_for
from flask import render_template
import pandas as pd
import os
import plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import json
from datetime import datetime
import numpy as np

app = Flask(__name__)


def create_plot(history, min_t, max_t):
    df24 = history[( datetime.now() - pd.to_datetime(history.time) ) / np.timedelta64(1, 'h') < 24]
    
    fig = go.Figure()

    fig.add_trace(
        go.Scatter(
                x=df24.time, 
                y=df24.temperature.rolling(5).mean(), 
                line=dict(color='#1f77b4')
            )
    )

    window_state = df24.window_state.replace({'window closed': df24.temperature.min(), 'window open': df24.temperature.max()})
    fig.add_trace(
        go.Scatter(
                x=df24.time, 
                y=window_state, 
                line=dict(color='red')
            )
    )

    fig.add_trace(
        go.Scatter(
            x=[df24.time.min(), df24.time.max()], 
            y=[min_t, min_t],
            mode='lines',
            line=dict(color='black')
        )
    )

    fig.add_trace(
        go.Scatter(
            x=[df24.time.min(), df24.time.max()], 
            y=[max_t, max_t],
            mode='lines',
            line=dict(color='black')
        )
    )

    fig.add_trace(
        go.Scatter(
            x=df24.time, 
            y=df24.humidity.rolling(5).mean(),
            yaxis="y2",
            line=dict(color='#ff7f0e')
        )
    )

    # Create axis objects
    fig.update_layout(
        # xaxis=dict(
        #     domain=[0.3, 0.7]
        # ),
        yaxis=dict(
            title="temperature",
            titlefont=dict(
                color="#1f77b4"
            ),
            tickfont=dict(
                color="#1f77b4"
            )
        ),
        yaxis2=dict(
            title="humidity",
            titlefont=dict(
                color="#ff7f0e"
            ),
            tickfont=dict(
                color="#ff7f0e"
            ),
            # anchor="free",
            overlaying="y",
            side="right",
            # position=0.15
        )
    )

    fig.update_layout(
        autosize=False, 
        width=300,
        height=300,
        title_text="Last 24h", 
        showlegend=False,
        margin=go.layout.Margin(
            l=0,
            r=5,
            b=10,
            t=50,
            pad=4
        ),
    )

    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    return graphJSON


@app.route('/')
def home():
    try:
        history = pd.read_csv('informations.csv')
        t = history.iloc[-1]['temperature']
        h = history.iloc[-1]['humidity']

        params = wm.get_config()
        min_t = params['min_t']
        max_t = params['max_t']

        mode = wm.get_mode()
    except:
        t=h=min_t=max_t=mode=""
    
    fig = create_plot(history, min_t, max_t)
    return render_template('base.html', temp=t, hum=h, min_t=min_t, max_t=max_t, mode=mode, plot=fig)


@app.route('/auto')
def auto():
    with open("mode.txt", "w") as f:
        f.write("auto")
    return redirect('/')

@app.route('/open')
def openw():
    with open("mode.txt", "w") as f:
        f.write("open")
    os.system('python3 /home/pi/projects/windowopener/window_manager.py')
    return redirect('/')

@app.route('/close')
def closew():
    with open("mode.txt", "w") as f:
        f.write("close")
    os.system('python3 /home/pi/projects/windowopener/window_manager.py')
    return redirect('/')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
