# Prepare the raspberry

## No way to run the wifi on the raspberry

1. Edit the file

`sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`

Put the content

```
country=FR
network={
  ssid="XXXX"
  psk="XXXX"
  key_mgmt=WPA-PSK
}
```

2. Run `sudo wpa_supplicant -Dwext -iwlan0 -c/etc/wpa_supplicant/wpa_supplicant.conf`

3. create crontab (`sudo crontab -e`) to be sure that this command will be run at each startup

`@reboot sudo wpa_supplicant -Dwext -iwlan0 -c/etc/wpa_supplicant/wpa_supplicant.conf`

4. Reboot: `sudo reboot`

## Set a fixed IP

check if dhcpcd is running

`sudo service dhcpcd status`

if not running:
```
sudo service dhcpcd start
sudo systemctl enable dhcpcd

sudo nano /etc/dhcpcd.conf
```

Put the content:

```
interface wifi0
static ip_address=192.168.0.10/24
static routers=192.168.0.1
static domain_name_servers=192.168.0.1
```

Here we chose the ip `192.168.0.10`, but we need to be sure that this IP is not already used.

## SSH connexion from remote computer with VS code

### Set SSH

Create an SSH key in your remote computer.

Add your ssh key to the file `/etc/hosts` in your raspberry.

Be carefull, if you work from Windows and create the key with puttygen, you will need to convert it to an RSA key before beeing able to use it.

From your local network, if you work in linux, you can now connect to it with `ssh pi@pyfenetre.local`, assuming that you named your raspberry `pyfenetre`, which I could perfectly understand.

### Set VS code

Install the `Remote Explorer` extension in VS code.

Click on the cross to add an SSH target, and enter the command `ssh pi@pyfenetre.local`.

This will add a configuration in your `~/.ssh/config` file, and open the ssh connexion.


# Start the server

`python3 app.py`

Create a crontab for the local user

`crontab -u pi -e`

`@reboot python3 /home/pi/projects/windowopener/window_manager.py`

# Run the window manager each minute
Create a crontab for the local user

`crontab -u pi -e`

`* * * * * cd /home/pi/projects/windowopener && python3 window_manager.py`

# Run the tests

`python3 -m pytest -v`